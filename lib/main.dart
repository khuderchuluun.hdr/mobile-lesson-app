import 'dart:async';
import 'package:flutter/material.dart';
import 'view/main/pages/splash_screen/splash_screen.dart';
import 'view/main/pages/onboarding/onboarding.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Splash Screen',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 3),
      () => Navigator.pushReplacement(context,
              MaterialPageRoute( builder: (context) =>FirstScreen() )
            )
    );
  }
  @override
  Widget build(BuildContext context) {
    return const SplashScreen();
  }

}
class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const OnboardingScreen();
  }
}