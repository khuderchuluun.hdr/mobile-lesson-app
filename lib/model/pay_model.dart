import 'package:flutter/foundation.dart';

class PayModel {
  final int? id;
  String? title;
  DateTime? date;
  String? image;
  double? amount;

  PayModel({
    @required this.id,
    @required this.title,
    @required this.date,
    @required this.image,
    @required this.amount,
  });
}
