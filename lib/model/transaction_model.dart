import 'package:flutter/foundation.dart';

class TransactionModel {
  final int? id;
  String? title;
  String? description;
  String? image;
  int? amount;

  TransactionModel({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.image,
    @required this.amount,
  });
}
