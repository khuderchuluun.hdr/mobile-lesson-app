
import 'package:community/view/main/pages/add_expense/add_expense.dart';
import 'package:community/view/main/pages/cash/cash.dart';
import 'package:community/view/main/pages/cash/sub_page/connect_wallet/connect_wallet.dart';
import 'package:community/view/main/pages/cash/wallet.dart';
import 'package:community/view/main/pages/home/home.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class MainPage extends StatefulWidget {
  final Widget currentPage;
  final int index;
  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = FirstPage();
  int currentTab = 0;
  MainPage(this.currentPage, this.index) {
    currentScreen = currentPage == null ? FirstPage() : currentPage;
    currentTab = index;
  }

  @override
  // ignore: library_private_types_in_public_api
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final List<Widget> screens = [
    FirstPage(),
    CashPage(WalletPage()),
  ];

  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        bucket: widget.bucket,
        child: widget.currentScreen,
      ),
      floatingActionButton: widget.currentTab == 0 ? FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: const Color.fromARGB(255, 9, 146, 127),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const AddExpense()),
          );
        },
      ) : null,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              //? Left button bar
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(
                    width: 20,
                  ),
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        widget.currentScreen = FirstPage();
                        widget.currentTab = 0;
                      });
                    },
                    child: Image(
                      image: AssetImage(
                        widget.currentTab == 0 ? 'nav_icons/home_active.png' : 'nav_icons/home.png'
                      )
                    ),
                  ),
                  SizedBox(
                    width: widget.currentTab == 0 ? 20 : 50,
                  ),
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        // currentScreen = CashPage();
                        widget.currentTab = 1;
                      });
                    },
                    child: const Image(
                      image: AssetImage('nav_icons/statistic.png')
                    ),
                  ),
                ],
              ),
              //? Right button bar
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        widget.currentScreen = CashPage(WalletPage());
                        widget.currentTab = 2;
                      });
                    },
                    child: Image(
                      image: AssetImage(
                        widget.currentTab == 2 ? 'nav_icons/wallet_active.png' : 'nav_icons/wallet.png'
                      )
                    ),
                  ),
                  SizedBox(
                    width: widget.currentTab == 0 ? 20 : 50,
                  ),
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        // currentScreen = CashPage();
                        widget.currentTab = 3;
                      });
                    },
                    child: const Image(image: AssetImage('nav_icons/user.png')),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}