import 'package:community/view/main/pages/home/home.dart';
import 'package:flutter/material.dart';
import '../../../container.dart';
import 'package:flutter/src/widgets/framework.dart';

class AddExpense extends StatefulWidget {
  const AddExpense({super.key});

  @override
  State<AddExpense> createState() => _AddExpenseState();
}

class _AddExpenseState extends State<AddExpense> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("Rectangle.png"),
            fit: BoxFit.fitWidth,
            alignment: Alignment.topCenter,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 60, 20, 20),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => MainPage(FirstPage(), 0)),
                      );
                    }, // Image tapped
                    child: const Image(
                      image: AssetImage('/chevron-left.png'),
                    ),
                  ),
                  
                  const Text(
                    "Add Expense",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  const Image(image: AssetImage('/dots.png')),
                ],
              ),
              const SizedBox(height: 50,),
              Center(
                child: SizedBox (
                  width: double.infinity,
                  height: 600,
                  child: Card(
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      )
                    ),
                    margin: const EdgeInsets.all(10),
                    color: Colors.white,
                    shadowColor: Colors.blueGrey,
                    elevation: 10.0,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(15, 20, 15, 20),
                      child: Row(
                        children: const [
                          
                        ],
                      ),
                    )
                  ),
                )
              )
            ],
          )
        )
      )
      
    );
  }
}