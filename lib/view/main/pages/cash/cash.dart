import 'package:community/view/main/pages/cash/sub_page/connect_wallet/connect_wallet.dart';
import 'package:community/view/main/pages/cash/wallet.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

import '../../../container.dart';
import '../../widgets/tab/tab.dart';

class CashPage extends StatefulWidget {
  final Widget currentPage;
  final PageStorageBucket bucket = PageStorageBucket();

  CashPage(this.currentPage);

  @override
  State<CashPage> createState() => _CashPageState();
}

class _CashPageState extends State<CashPage> {
  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
          bucket: bucket,
         child: widget.currentPage
      )
    );
  }
}