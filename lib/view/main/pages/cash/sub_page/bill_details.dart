import 'package:community/model/pay_model.dart';
import 'package:community/view/container.dart';
import 'package:community/view/main/pages/cash/cash.dart';
import 'package:community/view/main/pages/cash/sub_page/bill_payment.dart';
import 'package:community/view/main/widgets/pay_list/pay_list.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BillDetails extends StatefulWidget {
  final PayModel pay;
  var fee;
  var totalPrice;
  BillDetails(this.pay) {
    fee = pay.amount! * 10 / 100;
    totalPrice = pay.amount! + fee;
  }

  @override
  State<BillDetails> createState() => _BillDetailsState();
}

class _BillDetailsState extends State<BillDetails> {
  int? _value = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("Rectangle.png"),
            fit: BoxFit.fitWidth,
            alignment: Alignment.topCenter,
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 60, 20, 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    }, 
                    child: const Image(
                      image: AssetImage('/chevron-left.png'),
                    ),
                  ),
                  
                  const Text(
                    "Bill Details",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  const Image(image: AssetImage('/dots.png')),
                ],
              ),
            ),
            const SizedBox(height: 50,),
            Container(
              width: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 5)],
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 30, 20, 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(0), // Image border
                            child: SizedBox.fromSize(
                              child: Image(
                                image: AssetImage('${widget.pay.image!}'),
                                fit: BoxFit.cover
                                ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${widget.pay.title} Priemium",
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                                const SizedBox(height: 10,),
                                Text(
                                  DateFormat.yMMMd().format(widget.pay.date!),
                                  style: const TextStyle(
                                    color: Colors.grey,
                                    fontSize: 12
                                  ),
                                )
                              ],
                            )
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 30,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Price',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          '\$ ${widget.pay.amount}',
                        ),
                      ],
                    ),
                    const SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Fee',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          '\$ ${widget.fee}',
                        ),
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Container(
                      height: 1,
                      width: double.infinity,
                      color: Colors.grey,
                    ),
                    const SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Total',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          '\$ ${widget.totalPrice}',
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    const SizedBox(height: 50,),
                    const Text(
                      "Select payment method",
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    const SizedBox(height: 20,),

                    //? First radio
                    Container(
                      width: double.infinity,
                      height: 90,
                      decoration: BoxDecoration(
                        color: _value == 0 ? Color.fromARGB(31, 56, 139, 133) : Color.fromARGB(255, 227, 227, 227),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 15, right: 15, left: 15, bottom: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image(
                                  image: AssetImage(
                                    _value == 0 ? '/wallet/wallet_active.png' : '/wallet/wallet.png',
                                  ),
                                  fit: BoxFit.cover,
                                ),
                                const SizedBox(width: 20,),
                                Text(
                                  "Debit Card",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: _value == 0 ? Color.fromARGB(255, 56, 139, 133) : Colors.grey
                                  ),
                                ),
                              ],
                            ),
                            Radio(
                              value: 0,
                              activeColor: const Color.fromARGB(255, 56, 139, 133),
                              groupValue: _value,
                              onChanged: (value) {
                                setState(() {
                                  _value = value as int?;
                                });
                              },
                            )
                          ],
                        ),
                      )
                    ),
                    const SizedBox(height: 20,),

                    //? Second radio
                    Container(
                      width: double.infinity,
                      height: 90,
                      decoration: BoxDecoration(
                        color: _value == 1 ? Color.fromARGB(31, 56, 139, 133) : Color.fromARGB(255, 227, 227, 227),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 15, right: 15, left: 15, bottom: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                const SizedBox(width: 15,),
                                Image(
                                  image: AssetImage(
                                    _value == 1 ? '/wallet/paypal_active.png' : '/wallet/paypal.png',
                                  ),
                                  fit: BoxFit.cover,
                                ),
                                const SizedBox(width: 30,),
                                Text(
                                  "Paypal",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: _value == 1 ? Color.fromARGB(255, 56, 139, 133) : Colors.grey
                                  ),
                                ),
                              ],
                            ),
                            Radio(
                              value: 1,
                              activeColor: const Color.fromARGB(255, 56, 139, 133),
                              groupValue: _value,
                              onChanged: (value) {
                                setState(() {
                                  _value = value as int?;
                                });
                              },
                            )
                          ],
                        ),
                      )
                    ),
                    const SizedBox(height: 20,),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MainPage(CashPage(BillPayment(widget.pay, _value == 0 ? 'Debit Card' : 'Paypal')), 2)),
                        );
                      },
                      child: Container(
                        width: double.infinity,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          gradient: const LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Color.fromARGB(255, 0, 211, 183),
                              Color.fromARGB(255, 56, 139, 133),
                            ],
                          )
                        ),
                        child: const Center(
                          child: Text(
                            'Pay Now',
                            style: TextStyle(
                              fontSize: 20,
                              decoration: TextDecoration.none,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          )
                        ),
                      ),
                    ),
                  ],
                ),
              )
            )
          ],
        )
      )
      
    );
  }
}