import 'package:community/model/pay_model.dart';
import 'package:community/view/container.dart';
import 'package:community/view/main/pages/cash/cash.dart';
import 'package:community/view/main/pages/cash/sub_page/bill_success.dart';
import 'package:community/view/main/widgets/pay_list/pay_list.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BillPayment extends StatefulWidget {
  final PayModel pay;
  final String payMethod;
  var fee;
  var totalPrice;
  BillPayment(this.pay, this.payMethod) {
    fee = pay.amount! * 10 / 100;
    totalPrice = pay.amount! + fee;
  }

  @override
  State<BillPayment> createState() => _BillPaymentState();
}

class _BillPaymentState extends State<BillPayment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("Rectangle.png"),
            fit: BoxFit.fitWidth,
            alignment: Alignment.topCenter,
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 60, 20, 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    }, 
                    child: const Image(
                      image: AssetImage('/chevron-left.png'),
                    ),
                  ),
                  
                  const Text(
                    "Bill Payment",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  const Image(image: AssetImage('/dots.png')),
                ],
              ),
            ),
            const SizedBox(height: 50,),
            Container(
              width: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 5)],
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 30, 20, 20),
                child: Column(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(0), // Image border
                      child: SizedBox.fromSize(
                        child: Image(
                          image: AssetImage('${widget.pay.image!}'),
                          fit: BoxFit.cover
                          ),
                      ),
                    ),
                    const SizedBox(height: 30,),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(35, 0, 35, 0),
                      child: Center(
                        child: Text(
                          'You will pay ${widget.pay.title} Premium for one month with BCA OneKlik',
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 18
                          ),
                        ),
                      )
                    ),
                    const SizedBox(height: 30,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Price',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          '\$ ${widget.pay.amount}',
                        ),
                      ],
                    ),
                    const SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Fee',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          '\$ ${widget.fee}',
                        ),
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Container(
                      height: 1,
                      width: double.infinity,
                      color: Colors.grey,
                    ),
                    const SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Total',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          '\$ ${widget.totalPrice}',
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    const SizedBox(height: 200,),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MainPage(CashPage(BillSuccess(widget.pay, widget.payMethod)), 2)),
                        );
                      },
                      child: Container(
                        width: double.infinity,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          gradient: const LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Color.fromARGB(255, 0, 211, 183),
                              Color.fromARGB(255, 56, 139, 133),
                            ],
                          )
                        ),
                        child: const Center(
                          child: Text(
                            'Pay Now',
                            style: TextStyle(
                              fontSize: 20,
                              decoration: TextDecoration.none,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          )
                        ),
                      ),
                    ),
                  ],
                ),
              )
            )
          ],
        )
      )
      
    );
  }
}