import 'package:community/model/pay_model.dart';
import 'package:community/view/container.dart';
import 'package:community/view/main/pages/cash/cash.dart';
import 'package:community/view/main/pages/cash/wallet.dart';
import 'package:community/view/main/widgets/pay_list/pay_list.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BillSuccess extends StatefulWidget {
  final PayModel pay;
  final String payMethod;
  var fee;
  var totalPrice;
  
  BillSuccess(this.pay, this.payMethod) {
    fee = pay.amount! * 10 / 100;
    totalPrice = pay.amount! + fee;
  }

  @override
  State<BillSuccess> createState() => _BillSuccessState();
}

class _BillSuccessState extends State<BillSuccess> {
  final DateTime now = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("Rectangle.png"),
            fit: BoxFit.fitWidth,
            alignment: Alignment.topCenter,
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 60, 20, 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => CashPage(WalletPage())),
                      );
                    }, 
                    child: const Image(
                      image: AssetImage('/chevron-left.png'),
                    ),
                  ),
                  
                  const Text(
                    "Bill Payment",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  const Image(image: AssetImage('/dots.png')),
                ],
              ),
            ),
            const SizedBox(height: 50,),
            Container(
              width: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 5)],
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 20, 20, 5),
                child: Column(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(0), // Image border
                      child: SizedBox.fromSize(
                        child: const Image(
                          image: AssetImage('/wallet/success.png'),
                          fit: BoxFit.cover
                          ),
                      ),
                    ),
                    const SizedBox(height: 10,),
                    const Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                      child: Center(
                        child: Text(
                          'Payment Successfully',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20,
                            color: Color.fromARGB(255, 56, 139, 133)
                          ),
                        ),
                      )
                    ),
                    Text(
                      '${widget.pay.title} Premium',
                      style: const TextStyle(color: Colors.grey),
                    ),
                    const SizedBox(height: 35,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Text(
                          'Transaction details',
                          style: TextStyle(
                            fontSize: 17
                          ),
                        ),
                        Text(
                          '...',
                        ),
                      ],
                    ),
                    const SizedBox(height: 15,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        //? Pay method
                        const Text(
                          'Payment method',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          '${widget.payMethod}',
                        ),
                      ],
                    ),
                    const SizedBox(height: 10,),

                    //?Status
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Text(
                          'Status',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          'Completed',
                          style: TextStyle(color: Color.fromARGB(255, 56, 139, 133)),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10,),

                    //?Time
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Time',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          '${now.hour}:${now.minute}:${now.second}',
                        ),
                      ],
                    ),
                    const SizedBox(height: 10,),

                    //? Date
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Date',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          DateFormat.yMMMd().format(now),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10,),

                    //? Transaction ID
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Transaction ID',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Row(
                          children: const [
                            Text(
                              '2233554357854..',
                            ),
                            Icon(
                              Icons.copy,
                              size: 15,
                              color: Color.fromARGB(255, 56, 139, 133),
                            )
                          ],
                        )
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Container(
                      height: 1,
                      width: double.infinity,
                      color: Colors.grey,
                    ),
                    const SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Price',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          '\$ ${widget.pay.amount}',
                        ),
                      ],
                    ),
                    const SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Fee',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          '\$ ${widget.fee}',
                        ),
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Container(
                      height: 1,
                      width: double.infinity,
                      color: Colors.grey,
                    ),
                    const SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'Total',
                          style: TextStyle(color: Color.fromARGB(255, 74, 74, 74)),
                        ),
                        Text(
                          '\$ ${widget.totalPrice}',
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20,),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MainPage(CashPage(WalletPage()), 2)),
                        );
                      },
                      child: Container(
                        width: double.infinity,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          border: Border.all(width: 1, color: const Color.fromARGB(255, 56, 139, 133),),
                        ),
                        child: const Center(
                          child: Text(
                            'Share Receipt',
                            style: TextStyle(
                              fontSize: 20,
                              decoration: TextDecoration.none,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 56, 139, 133),
                            ),
                          )
                        ),
                      ),
                    ),
                  ],
                ),
              )
            )
          ],
        )
      )
      
    );
  }
}