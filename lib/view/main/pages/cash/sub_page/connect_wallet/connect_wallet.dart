import 'package:community/model/pay_model.dart';
import 'package:community/model/transaction_model.dart';
import 'package:community/view/main/widgets/add_cards/add_cards.dart';
import 'package:community/view/main/widgets/pay_list/pay_list.dart';
import 'package:community/view/main/widgets/tab/tab.dart';
import 'package:community/view/main/widgets/transaction/transaction.dart';
import 'package:community/view/main/widgets/wallet_tab/wallet_tab.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class ConnectWallet extends StatefulWidget {
  const ConnectWallet({super.key});

  @override
  State<ConnectWallet> createState() => _ConnectWalletState();
}

class _ConnectWalletState extends State<ConnectWallet> {
  int currentTab = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("Rectangle.png"),
            fit: BoxFit.fitWidth,
            alignment: Alignment.topCenter,
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 60, 20, 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    }, // Image tapped
                    child: const Image(
                      image: AssetImage('/chevron-left.png'),
                    ),
                  ),
                  
                  const Text(
                    "Connect Wallet",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  const Icon(
                    LineIcons.bell,
                    color: Colors.white,
                  )
                ],
              ),
            ),
            const SizedBox(height: 50,),
            Container(
              width: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 5)],
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 30, 20, 15),
                child: Column(
                  children: [
                    CardTab()
                  ],
                ),
              )
            )
          ],
        )
      );
  }
}