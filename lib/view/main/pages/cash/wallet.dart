
import 'package:community/view/main/pages/cash/cash.dart';
import 'package:community/view/main/pages/cash/sub_page/connect_wallet/connect_wallet.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:line_icons/line_icons.dart';

import '../../../container.dart';
import '../../widgets/tab/tab.dart';
import '../../widgets/wallet_tab/wallet_tab.dart';

class WalletPage extends StatefulWidget {
  const WalletPage({super.key});

  @override
  State<WalletPage> createState() => _WalletPageState();
}

class _WalletPageState extends State<WalletPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("Rectangle.png"),
          fit: BoxFit.fitWidth,
          alignment: Alignment.topCenter,
        ),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 60, 20, 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(builder: (context) => MainPage()),
                    // );
                  }, // Image tapped
                  child: const Image(
                    image: AssetImage('/chevron-left.png'),
                  ),
                ),
                
                const Text(
                  "Wallet",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
                const Icon(
                  LineIcons.bell,
                  color: Colors.white,
                )
              ],
            ),
          ),
          const SizedBox(height: 50,),
          Container(
            width: double.infinity,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
              boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 5)],
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 30, 20, 15),
              child: Column(
                children: [
                  const Text(
                    "Total Balance",
                    style: TextStyle(
                      color: Color.fromARGB(255, 116, 116, 116),
                    ),
                  ),
                  const SizedBox(height: 10,),
                  const Text(
                    "\$2,500.00",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 27,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50, 30, 50, 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => MainPage(CashPage(const ConnectWallet()), 2)),
                            );
                          }, // Image tapped
                          child: const Image(
                            image: AssetImage('/wallet/add.png'),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            // Navigator.push(
                            //   context,
                            //   MaterialPageRoute(builder: (context) => const MainPage()),
                            // );
                          }, // Image tapped
                          child: const Image(
                            image: AssetImage('/wallet/pay.png'),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            // Navigator.push(
                            //   context,
                            //   MaterialPageRoute(builder: (context) => const MainPage()),
                            // );
                          }, // Image tapped
                          child: const Image(
                            image: AssetImage('/wallet/send.png'),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const WalletTab(),
                ],
              ),
            )
          )
        ],
      )
    );
  }
}