import 'package:community/view/main/widgets/instagram_story/insta_story.dart';
import 'package:community/view/main/widgets/transaction/transaction.dart';
import 'package:flutter/material.dart';
import 'package:awesome_card/awesome_card.dart';
import 'package:line_icons/line_icons.dart';
import 'package:money_formatter/money_formatter.dart';  
import '../../../../model/transaction_model.dart';
import '../../widgets/card/card.dart';
import 'package:intl/intl.dart';

class FirstPage extends StatefulWidget {
  final double amount = 2500.00;
  final double income = 1800.00;
  late final double expenses;
  FirstPage({super.key}) {
    expenses = amount - income;
  }

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  final List<TransactionModel> trans = [
    TransactionModel(
      id: 1, 
      title: "Upwork", 
      description: "Today", 
      image: "/pay_list/youtube.png", 
      amount: 1000
    ),
    TransactionModel(
      id: 2, 
      title: "Upwork", 
      description: "Today", 
      image: "/pay_list/house.png", 
      amount: -1000
    ),
  ];
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("Rectangle.png"),
            fit: BoxFit.fitWidth,
            alignment: Alignment.topCenter,
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 60, 20, 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        "Good afternoon,",
                        style: TextStyle(color: Colors.white),
                      ),
                      Text(
                        "Khuderchuluun Alimaa",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                        ),
                      )
                    ],
                  ),
                  const Icon(
                    LineIcons.bell,
                    color: Colors.white,
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 30.0,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              child: ElevatedCardExample(
                total_amount: widget.amount,
                income: widget.income,
                expenses: widget.expenses,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Text(
                        "Transaction History",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                      Text(
                        "See all",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                        ),
                      )
                    ],
                  ),
                  TransactionList(trans),
                  const SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Text(
                        "Send Again",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                      Text(
                        "See all",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  const InstaStory()
                ],
              ),
            ),
          ],
        )
      )
      
    );
  }
}