import 'package:community/view/main/pages/home/home.dart';
import 'package:flutter/material.dart';
import '../../../container.dart';

class OnboardingScreen extends StatelessWidget {
  const OnboardingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Stack(
            children: <Widget>[
              Container(
                height: 550,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("background.png"), 
                    fit: BoxFit.cover,
                  ),
                ),
                child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Container(
                    height: 480,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("human.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            width: double.infinity,
            height: 30,
          ),
          const Center(
            child: Text(
              'Spend Smarter Save More',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 45.0, 
                decoration: TextDecoration.none,
                fontWeight: FontWeight.bold,
                color: Color.fromARGB(255, 9, 146, 127),
              ),
            ),
          ),
          const SizedBox(
            width: double.infinity,
            height: 20,
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MainPage(FirstPage(), 0)),
              );
            },
            child: Container(
              width: 350,
              height: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                gradient: const LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color.fromARGB(255, 0, 211, 183),
                    Color.fromARGB(255, 0, 116, 100),
                  ],
                )
              ),
              child: const Center(
                child: Text(
                  'Get Started',
                  style: TextStyle(
                    fontSize: 20,
                    decoration: TextDecoration.none,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                )
              ),
            ),
          ),
          const SizedBox(
            width: double.infinity,
            height: 20,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Text(
                'Already Have Account?',
                style: TextStyle(
                  fontSize: 15,
                  decoration: TextDecoration.none,
                  color: Colors.black,
                ),
              ),
              Text(
                ' Log In',
                style: TextStyle(
                  fontSize: 15,
                  decoration: TextDecoration.none,
                  color: Color.fromARGB(255, 0, 211, 183),
                ),
              ),
            ],
          )
        ], 
      ),
    );
  }
}