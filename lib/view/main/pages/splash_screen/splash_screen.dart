import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromARGB(255, 0, 211, 183),
            Color.fromARGB(255, 0, 116, 100),
          ],
        )
      ),
      child: const Center(
        child: Text(
          'mono',
          style: TextStyle(
            fontSize: 48.0,
            decoration: TextDecoration.none,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}