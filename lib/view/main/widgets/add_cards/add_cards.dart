
import 'package:awesome_card/credit_card.dart';
import 'package:awesome_card/extra/card_type.dart';
import 'package:awesome_card/style/card_background.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class AddCards extends StatefulWidget {
  const AddCards({super.key});

  @override
  State<AddCards> createState() => _AddCardsState();
}

class _AddCardsState extends State<AddCards> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.all(20),
            child: Center(
              child: Image(image: AssetImage('Cards.png'))
            ),
          ),
          const SizedBox(height: 20,),
          const Text(
            'Add your debit Card',
            style: TextStyle(fontSize: 17),
          ),
          const Text(
            'This card must be connected to a bank account under your name',
            style: TextStyle(color: Colors.grey),
          ),
          const SizedBox(height: 40,),
          Container(
            width: double.infinity,
            child: const TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    width: 1,
                    color: Color.fromARGB(255, 56, 139, 133),
                  )
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    width: 1,
                    color: Color.fromARGB(255, 56, 139, 133),
                  )
                ),
                labelText: 'NAME ON CARD',
                hintText: 'NAME ON CARD',
                labelStyle: TextStyle(color: Colors.grey)
              ),
              autofocus: false,
            )
          ),
          const SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 220,
                child: const TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 1,
                        color: Color.fromARGB(255, 56, 139, 133),
                      )
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 1,
                        color: Color.fromARGB(255, 56, 139, 133),
                      )
                    ),
                    labelText: 'DEBIT CARD NUMBER',
                    hintText: 'DEBIT CARD NUMBER',
                    labelStyle: TextStyle(color: Colors.grey)
                  ),
                  autofocus: false,
                )
              ),
              SizedBox(width: 10,),
              Container(
                width: 120,
                child: const TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 1,
                        color: Color.fromARGB(255, 56, 139, 133),
                      )
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 1,
                        color: Color.fromARGB(255, 56, 139, 133),
                      )
                    ),
                    labelText: 'CVC',
                    hintText: 'CVC',
                    labelStyle: TextStyle(color: Colors.grey)
                  ),
                  autofocus: false,
                )
              ),
            ],
          ),
          const SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 220,
                child: const TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 1,
                        color: Color.fromARGB(255, 56, 139, 133),
                      )
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 1,
                        color: Color.fromARGB(255, 56, 139, 133),
                      )
                    ),
                    labelText: 'EXPIRATION MM/YY',
                    hintText: 'EXPIRATION MM/YY',
                    labelStyle: TextStyle(color: Colors.grey)
                  ),
                  autofocus: false,
                )
              ),
              SizedBox(width: 10,),
              Container(
                width: 120,
                child: const TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 1,
                        color: Color.fromARGB(255, 56, 139, 133),
                      )
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 1,
                        color: Color.fromARGB(255, 56, 139, 133),
                      )
                    ),
                    labelText: 'ZIP',
                    hintText: 'ZIP',
                    labelStyle: TextStyle(color: Colors.grey)
                  ),
                  autofocus: false,
                )
              ),
            ],
          )
        ],
      ),
    );
  }
}