import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class ElevatedCardExample extends StatefulWidget {
  const ElevatedCardExample(
    {
      Key? key, 
      required this.total_amount,
      required this.income,
      required this.expenses,
    }) : super(key: key);
  final double total_amount;
  final double income;
  final double expenses;

  @override
  State<ElevatedCardExample> createState() => _ElevatedCardExampleState();
}
class _ElevatedCardExampleState extends State<ElevatedCardExample> {
  @override
  Widget build(BuildContext context) {
    // Use the Todo to create the UI.
    return Center(
      child: SizedBox (
        width: double.infinity,
        height: 200,
        child: Card(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            )
          ),
          margin: const EdgeInsets.all(10),
          color: const Color.fromARGB(255, 9, 146, 127),
          shadowColor: Colors.blueGrey,
          elevation: 10.0,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(15, 20, 15, 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const <Widget>[
                        Text(
                          "Total Blance",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                          ),
                        ),
                        Image(image: AssetImage('dots.png')),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Text(
                        '\$ ${widget.total_amount}',
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 27,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: const [
                            Image(image: AssetImage('card/bottom.png')),
                            Text(
                              " Income",
                              style: TextStyle(
                                color: Colors.white
                              ),
                            )
                          ],
                        ),
                        Text(
                          '\$ ${widget.income}',
                          style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: const [
                            Image(image: AssetImage('card/top.png')),
                            Text(
                              " Expenses",
                              style: TextStyle(
                                color: Colors.white
                              ),
                            )
                          ],
                        ),
                        Text(
                          '\$ ${widget.expenses}',
                          style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
          )
        ),
      )
    );
  }
}
