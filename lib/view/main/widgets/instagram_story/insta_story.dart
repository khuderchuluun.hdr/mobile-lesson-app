import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class InstaStory extends StatefulWidget {
  const InstaStory({super.key});

  @override
  State<InstaStory> createState() => _InstaStoryState();
}

class _InstaStoryState extends State<InstaStory> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(100), // Image border
          child: SizedBox.fromSize(
            child: const Image(
              image: AssetImage('/insta_story/1.png'),
              fit: BoxFit.cover
              ),
          ),
        ),
        ClipRRect(
          borderRadius: BorderRadius.circular(100), // Image border
          child: SizedBox.fromSize(
            child: const Image(
              image: AssetImage('/insta_story/2.png'),
              fit: BoxFit.cover
              ),
          ),
        ),
        ClipRRect(
          borderRadius: BorderRadius.circular(100), // Image border
          child: SizedBox.fromSize(
            child: const Image(
              image: AssetImage('/insta_story/3.png'),
              fit: BoxFit.cover
              ),
          ),
        ),
        ClipRRect(
          borderRadius: BorderRadius.circular(100), // Image border
          child: SizedBox.fromSize(
            child: const Image(
              image: AssetImage('/insta_story/4.png'),
              fit: BoxFit.cover
              ),
          ),
        ),
        ClipRRect(
          borderRadius: BorderRadius.circular(100), // Image border
          child: SizedBox.fromSize(
            child: const Image(
              image: AssetImage('/insta_story/5.png'),
              fit: BoxFit.cover
              ),
          ),
        ),
      ],
    );
  }
}