import 'package:community/model/pay_model.dart';
import 'package:community/view/container.dart';
import 'package:community/view/main/pages/cash/cash.dart';
import 'package:community/view/main/pages/cash/sub_page/bill_details.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

void main() async {
  // WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();
  // var url = Uri.https('https://community-58df7-default-rtdb.firebaseio.com/', './pay_list.json');
  // var response = await http.get(url);
  // print(response);
  // runApp(PayList());
}

class PayList extends StatefulWidget {
  PayList({super.key});
  @override
  State<PayList> createState() => _PayListState();
}

class _PayListState extends State<PayList> {
  final List<PayModel> pays = [
    PayModel(
      id: 1, 
      title: "Youtube", 
      date: DateTime.now(), 
      image: '/pay_list/youtube.png',
      amount: 11.99,
    ),
    PayModel(
      id: 2, 
      title: "Electricity", 
      date: DateTime.now(), 
      image: '/pay_list/elec.png',
      amount: 12.00
    ),
    PayModel(
      id: 3, 
      title: "House Rent", 
      date: DateTime.now(), 
      image: '/pay_list/house.png',
      amount: 15.03
    ),
    PayModel(
      id: 1, 
      title: "Spotify", 
      date: DateTime.now(), 
      image: '/pay_list/spotify.png',
      amount: 11.56
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: pays.map((t) {
            return Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(0), // Image border
                            child: SizedBox.fromSize(
                              child: Image(
                                image: AssetImage('${t.image!}'),
                                fit: BoxFit.cover
                                ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${t.title}",
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                                const SizedBox(height: 10,),
                                Text(
                                  DateFormat.yMMMd().format(t.date!),
                                  style: const TextStyle(
                                    color: Colors.grey,
                                    fontSize: 12
                                  ),
                                )
                              ],
                            )
                          )
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MainPage(CashPage(BillDetails(t)), 2)),
                        );
                        main();
                      }, // Image tapped
                      child: const Image(
                        image: AssetImage('/pay_list/pay_button.png'),
                      ),
                    ),
                  ],
                ),
              )
            );
          }).toList(),
        ),
      ),
    );
  }
}