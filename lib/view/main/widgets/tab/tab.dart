import 'package:community/model/pay_model.dart';
import 'package:community/model/transaction_model.dart';
import 'package:community/view/main/widgets/pay_list/pay_list.dart';
import 'package:community/view/main/widgets/transaction/transaction.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';

class WalletTab extends StatefulWidget {
  const WalletTab({super.key});

  @override
  State<WalletTab> createState() => _WalletTabState();
}

class _WalletTabState extends State<WalletTab> {
  int currentTab = 0;

  final List<TransactionModel> trans = [
    TransactionModel(
      id: 1, 
      title: "Upwork", 
      description: "Today", 
      image: "/pay_list/youtube.png", 
      amount: 1000
    ),
    TransactionModel(
      id: 2, 
      title: "Upwork", 
      description: "Today", 
      image: "/pay_list/house.png", 
      amount: -1000
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          decoration: const BoxDecoration(
            color: Color.fromARGB(255, 239, 239, 239),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(3),
            child:  Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      currentTab = 0;
                    });
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2.3,
                    height: 35,
                    decoration: BoxDecoration(
                      color: currentTab == 0 ? Colors.white : null,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30),
                      ),
                    ),
                    child: const Center(
                      child: Text(
                        "Transactions",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 116, 116, 116)
                        ),
                      ),
                    )
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      currentTab = 1;
                    });
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2.3,
                    height: 35,
                    decoration: BoxDecoration(
                      color: currentTab == 1 ? Colors.white : null,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30),
                      ),
                    ),
                    child: const Center(
                      child: Text(
                        "Upcomming Bills",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 116, 116, 116)
                        ),
                      ),
                    )
                  ),
                ),
              ],
            ),
          )
        ),
        currentTab == 0 ? TransactionList(trans) : PayList(),
      ],
    );
  }
}