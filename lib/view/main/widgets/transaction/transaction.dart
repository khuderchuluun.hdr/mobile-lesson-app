
import 'package:community/model/transaction_model.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';

class TransactionList extends StatelessWidget {
  final List<TransactionModel> trans;
  TransactionList(this.trans);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: trans.map((t) {
            return Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(0), // Image border
                            child: SizedBox.fromSize(
                              child: Image(
                                image: AssetImage('${t.image!}'),
                                fit: BoxFit.cover
                                ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${t.title}",
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                                const SizedBox(height: 10,),
                                Text(
                                  '${t.description}',
                                  style: const TextStyle(
                                    color: Colors.grey,
                                    fontSize: 12
                                  ),
                                )
                              ],
                            )
                          )
                        ],
                      ),
                    ),
                    Text(
                      '\$${t.amount}',
                      style: TextStyle(
                        color: t.amount! > 0 ? Colors.green : Colors.red,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              )
            );
          }).toList(),
        ),
      ),
    );
  }
}