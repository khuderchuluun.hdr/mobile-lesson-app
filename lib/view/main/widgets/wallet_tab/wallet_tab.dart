import 'package:community/model/pay_model.dart';
import 'package:community/model/transaction_model.dart';
import 'package:community/view/main/widgets/add_cards/add_cards.dart';
import 'package:community/view/main/widgets/pay_list/pay_list.dart';
import 'package:community/view/main/widgets/transaction/transaction.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';

class CardTab extends StatefulWidget {
  const CardTab({super.key});

  @override
  State<CardTab> createState() => _CardTabState();
}

class _CardTabState extends State<CardTab> {
  int currentTab = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          decoration: const BoxDecoration(
            color: Color.fromARGB(255, 239, 239, 239),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(3),
            child:  Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      currentTab = 0;
                    });
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2.3,
                    height: 35,
                    decoration: BoxDecoration(
                      color: currentTab == 0 ? Colors.white : null,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30),
                      ),
                    ),
                    child: const Center(
                      child: Text(
                        "Cards",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 116, 116, 116)
                        ),
                      ),
                    )
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      currentTab = 1;
                    });
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2.3,
                    height: 35,
                    decoration: BoxDecoration(
                      color: currentTab == 1 ? Colors.white : null,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30),
                      ),
                    ),
                    child: const Center(
                      child: Text(
                        "Account",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 116, 116, 116)
                        ),
                      ),
                    )
                  ),
                ),
              ],
            ),
          )
        ),
        currentTab == 0 ? AddCards() : PayList(),
      ],
    );
  }
}